package g.jr.task4;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class LoginActivity extends AppCompatActivity {

    Button random;
    Button submit;
    TextView usernameField;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        random = findViewById(R.id.randomButton);
        submit = findViewById(R.id.submitButton);
        usernameField = findViewById(R.id.userNameField);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = usernameField.getText().toString();

                if (username.isEmpty()) {
                    // error
                }
                else {
                    Intent intent = new Intent();
                    intent.putExtra("usernameIntent", username);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }
            }
        });
    }
}
