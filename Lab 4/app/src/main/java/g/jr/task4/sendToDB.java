package g.jr.task4;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class sendToDB {
    public String message;
    public String date;
    public String user;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    sendToDB() {

    }

    sendToDB(String sendToDB, String username) {
        this.message = sendToDB;
        Date date = new Date();
        SimpleDateFormat sfd = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.UK);
        this.date = sfd.format(date);
        this.user = username;
    }
}
