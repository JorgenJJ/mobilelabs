package g.jr.task4;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Collections;

public class UserList extends android.support.v4.app.Fragment {

    ListView userList;
    private DatabaseReference ref;
    private ArrayList<String> userArray;
    private ArrayAdapter<String> adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_user_list, container, false);

            userList = rootView.findViewById(R.id.userList);
            userArray = new ArrayList<>();
            adapter = new ArrayAdapter<>(getContext(), R.layout.chat_item, userArray);
            userList.setAdapter(adapter);

            ref = FirebaseDatabase.getInstance().getReference();

            userList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent intent = new Intent();
                    String string = (String)adapterView.getItemAtPosition(i);
                    intent.putExtra("user", string);
                }
            });

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                for (DataSnapshot ds: dataSnapshot.getChildren()) {
                    sendToDB users = ds.getValue(sendToDB.class);
                    assert users != null;

                    if(!userArray.contains(users.getUser())) {
                        userArray.add(users.getUser());
                        Collections.sort(userArray);
                        adapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
