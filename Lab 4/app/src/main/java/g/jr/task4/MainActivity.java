package g.jr.task4;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    private boolean first = true;
    public static String username;
    private int result = 123;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        checkFirstLaunch();
        username = getUsername();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        ViewPager viewPager = findViewById(R.id.container);
        viewPager.setAdapter(sectionsPagerAdapter);

        TabLayout tabLayout = findViewById(R.id.tabs);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));

        auth = FirebaseAuth.getInstance();

        signInAnonymously();

    }

    private void checkFirstLaunch() {
        if (first) {
            SharedPreferences fTimePref = this.getSharedPreferences("firstTime", Context.MODE_PRIVATE);
            first = fTimePref.getBoolean("firstTime", true);
            if (first) {
                Intent intent = new Intent(this, LoginActivity.class);
                startActivityForResult(intent, result);
            }
        }
    }

    private String getUsername() {
        String username;
        SharedPreferences unamePref = this.getSharedPreferences("username", Context.MODE_PRIVATE);
        username = unamePref.getString("username", "DefaultUser");
        return username;
    }

    private void signInAnonymously() {
        auth.signInAnonymously()
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("mainActivity", "signInAnonymously:success");
                            FirebaseUser user = auth.getCurrentUser();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("mainActivity", "signInAnonymously:failure", task.getException());
                        }
                    }
                });
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = auth.getCurrentUser();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (result == requestCode) {
            if (resultCode == RESULT_OK) {
                SharedPreferences first = this.getSharedPreferences("firstLaunch", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = first.edit();
                editor.putBoolean("firstLaunch", false);
                editor.apply();

                String usernameActivity = intent.getStringExtra("usernameIntent");
                SharedPreferences saveUsername = this.getSharedPreferences("username", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor2 = saveUsername.edit();
                editor2.putString("username", usernameActivity);
                editor2.apply();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    //Uses a bundle to send constructor data since a constructor can't be used in a fragment
                    Bundle chatBundle = new Bundle();
                    Log.d("TEST", "Sets username in the bundle: " + username);
                    chatBundle.putString("username", getUsername()); //Sends the username

                    ChatActivity chatFragment = new ChatActivity();
                    chatFragment.setArguments(chatBundle);  //Attaches the username to the fragment. Have to be done before the fragment is added to the fragmentManager

                    return chatFragment;

                case 1:
                    UserList usersFragment = new UserList();
                    return usersFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }
    }

    public Context getContext() { return this; }
}
