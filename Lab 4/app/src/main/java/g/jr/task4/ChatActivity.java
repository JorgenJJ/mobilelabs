package g.jr.task4;

import android.support.annotation.Nullable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Objects;

public class ChatActivity extends android.support.v4.app.Fragment {

    private String username;
    Button submit;
    TextView message;
    ListView listView;

    private DatabaseReference ref;
    private ArrayList<String> messageItems;
    private ArrayAdapter<String> arrayAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_chat, container, false);

        message = rootView.findViewById(R.id.message);
        listView = rootView.findViewById(R.id.listView);

        messageItems = new ArrayList<>();
        arrayAdapter = new ArrayAdapter<>(getContext(), R.layout.chat_item, messageItems);
        listView.setAdapter(arrayAdapter);

        ref = FirebaseDatabase.getInstance().getReference();

        Bundle bundle = getArguments();
        username = bundle.getString("username");

        submit = rootView.findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (submitMessage()) message.setText(null);
            }
        });

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                for (DataSnapshot ds: dataSnapshot.getChildren()) {
                    sendToDB msg = ds.getValue(sendToDB.class);
                    assert msg != null;
                    messageItems.add(msg.getUser() + ": " + msg.getMessage());
                    arrayAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }

    private boolean submitMessage(){
        String msg = message.getText().toString();
        if(Objects.equals(msg, "")){
            Log.d("TEST", "Submit failed (EMPTY MESSAGE)");
            return false;
        }else {
            sendToDB messages = new sendToDB(msg, username);
            ref.push().child("messages").setValue(messages);

            return true;
        }
    }
}
