package jrgn.task3;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Vibrator;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.FrameLayout;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends Activity {

    BallView ballView = null;
    Handler RedrawHandler = new Handler();
    Timer timer = null;
    TimerTask timerTask = null;
    android.graphics.PointF pos, vel;
    float width = Resources.getSystem().getDisplayMetrics().widthPixels;
    float height = Resources.getSystem().getDisplayMetrics().heightPixels;
    Vibrator v;
    Uri ding;
    Ringtone ring;
    int alreadyTouching = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        final FrameLayout mainView = (android.widget.FrameLayout)findViewById(R.id.main_view);

        v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        ding = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        ring = RingtoneManager.getRingtone(getApplicationContext(), ding);

        pos = new android.graphics.PointF();
        vel = new android.graphics.PointF();

        pos.x = (height / 2);
        pos.y = (width / 2);
        vel.x = 0;
        vel.y = 0;

        ballView = new BallView(this, pos.x, pos.y);
        mainView.addView(ballView);
        ballView.invalidate();

        ((SensorManager)getSystemService(Context.SENSOR_SERVICE)).registerListener(
                new SensorEventListener() {
                    @Override
                    public void onSensorChanged(SensorEvent sensorEvent) {
                        vel.x = sensorEvent.values[1];
                        vel.y = sensorEvent.values[0];
                    }

                    @Override
                    public void onAccuracyChanged(Sensor sensor, int i) {}
                },
                ((SensorManager)getSystemService(Context.SENSOR_SERVICE))
                        .getSensorList(Sensor.TYPE_ACCELEROMETER).get(0),
                SensorManager.SENSOR_DELAY_NORMAL);

        mainView.setOnTouchListener(new android.view.View.OnTouchListener() {
            public boolean onTouch(android.view.View v, android.view.MotionEvent e) {
                pos.x = e.getX();
                pos.y = e.getY();
                return true;
            }});

    }

    @Override
    public void onPause() {
        timer.cancel();
        timer = null;
        timerTask = null;
        super.onPause();
    }

    @Override
    public void onResume() {
        timer = new Timer();
        timerTask = new TimerTask() {
            public void run() {

                android.util.Log.d(
                        "Task3","Timer Hit - " + pos.x + ":" + pos.y);

                pos.x += vel.x;
                pos.y += vel.y;

                float r = (float)((Resources.getSystem().getDisplayMetrics().widthPixels * 0.05) + 25);

                if (pos.x > width - r) {
                    pos.x -= vel.x;
                    if (alreadyTouching == 0) {
                        v.vibrate(100);
                        ring.play();
                    }
                    alreadyTouching = 1;
                }
                if (pos.y > height - r) {
                    pos.y -= vel.y;
                    if (alreadyTouching == 0) {
                        v.vibrate(100);
                        ring.play();
                    }
                    alreadyTouching = 1;
                }
                if (pos.x < 0 + r) {
                    pos.x -= vel.x;
                    if (alreadyTouching == 0) {
                        v.vibrate(100);
                        ring.play();
                    }
                    alreadyTouching = 1;
                }
                if (pos.y < 0 + r) {
                    pos.y -= vel.y;
                    if (alreadyTouching == 0) {
                        v.vibrate(100);
                        ring.play();
                    }
                    alreadyTouching = 1;
                }

                if ((pos.x < width - r - 5) && (pos.y < height - r - 5) &&
                        (pos.x > 0 + r + 5) && (pos.y > 0 + r + 5))
                    alreadyTouching = 0;

                ballView.x = pos.x;
                ballView.y = pos.y;

                RedrawHandler.post(new Runnable() {
                    public void run() {
                        ballView.invalidate();
                    }
                });
            }
        };
        timer.schedule(timerTask, 10, 10);
        super.onResume();
    }
}
