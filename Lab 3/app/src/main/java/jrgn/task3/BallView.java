package jrgn.task3;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

public class BallView extends View {
    private Rect rectangle;
    private Paint paintBackground;
    public float x;
    public float y;
    private float r = (float)(Resources.getSystem().getDisplayMetrics().widthPixels * 0.05);
    private Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

    public BallView(Context context, float x, float y) {
        super(context);

        int xx = 30;
        int yy = 30;
        int width = Resources.getSystem().getDisplayMetrics().widthPixels;
        int height = Resources.getSystem().getDisplayMetrics().heightPixels;

        rectangle = new Rect(xx, yy, width - xx, height - yy);

        paintBackground = new Paint();
        paintBackground.setColor(Color.BLACK);

        paint.setColor(Color.WHITE);
        this.x = x;
        this.y = y;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRect(rectangle, paintBackground);
        canvas.drawCircle(x, y, r, paint);
    }
}
