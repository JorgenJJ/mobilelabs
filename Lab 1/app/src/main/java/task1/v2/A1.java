package task1.v2;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Button;
import android.content.Intent;

public class A1 extends AppCompatActivity {

    String [] list = new String[]{"Adolf", "Brudolf", "Codolf"};
    Intent A1Intent = new Intent();
    SharedPreferences rememberSelection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a1);
        rememberSelection = this.getSharedPreferences("sharedpreference", 0);

        EditText T1 = (EditText) findViewById(R.id.T1);
        T1.setText(rememberSelection.getString("string", "")); // Sets the EditText to be what it was last session


        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, list);

        final Spinner dropdown = findViewById(R.id.L1);
        dropdown.setAdapter(arrayAdapter);
        dropdown.setSelection(rememberSelection.getInt("selection", 0));    // Makes dropdown selection what was last selected

        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                String selection = parent.getItemAtPosition(pos).toString();    // Stores the selection as a string

                SharedPreferences.Editor editor = rememberSelection.edit();     // Stores the selection in SharedPreferences
                editor.putInt("selection", pos);
                editor.putString("selectionString", selection);
                editor.commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // NOTHING HERE
            }

        });

        Button btn = (Button)findViewById(R.id.B1);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText content = (EditText) findViewById(R.id.T1);
                String contentString = content.getText().toString();    // Converts what the user wrote into string
                SharedPreferences.Editor editor = rememberSelection.edit();    // Adds the text to SharedPreferences
                editor.putString("string", contentString).commit();

                Intent stringIntent = new Intent(A1.this, A2.class);
                stringIntent.putExtra("string", contentString);    // Adds selection to intent as a string

                startActivity(stringIntent);    // Opens activity to and sends Intent
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
