package task1.v2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class A2 extends AppCompatActivity {

    TextView mTextView;
    String spinnerSelection;
    SharedPreferences rememberSelection;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a2);
        rememberSelection = this.getSharedPreferences("sharedpreference", 0);

        TextView T2 = (TextView) findViewById(R.id.T2);     // Gets the saved values from previous sessions
        T2.setText("Hello " + rememberSelection.getString("string", ""));
        TextView T3 = (TextView) findViewById(R.id.T3);     // ---------------||----------------
        T3.setText("From A3: " + rememberSelection.getString("string2", ""));


        mTextView = (TextView)findViewById(R.id.T2);
        if (getIntent().getStringExtra("Spinner") != null) {
            spinnerSelection = getIntent().getStringExtra("Spinner");   // Gets the spinner selection
            mTextView.setText("Hello " + spinnerSelection);                   // Displays it
        }

        Button btn = (Button)findViewById(R.id.B2);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityIntent = new Intent(v.getContext(), A3.class);
                startActivityForResult(activityIntent, 1);  // Starts activity 3
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(A2.this, A1.class));
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == A2.RESULT_OK) {   // If activity 3 gave a result
                String result=data.getStringExtra("result");    // Makes the result into a string

                TextView T3 = (TextView) findViewById(R.id.T3);
                T3.setText("From A3: " + result);       // Displays the result
            }
        }
    }
}
