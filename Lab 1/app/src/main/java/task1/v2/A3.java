package task1.v2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class A3 extends AppCompatActivity {

    SharedPreferences rememberString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a3);
        rememberString = this.getSharedPreferences("sharedpreference", 0);

        EditText T4 = (EditText) findViewById(R.id.T4);
        T4.setText(rememberString.getString("string2", "")); // Sets the EditText to be what it was last session


        Button btn = (Button)findViewById(R.id.B3);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText content = (EditText) findViewById(R.id.T4);
                String contentString = content.getText().toString();    // Converts what the user wrote into string
                SharedPreferences.Editor editor = rememberString.edit();    // Adds the text to SharedPreferences
                editor.putString("string2", contentString).commit();

                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", contentString); // Returns to activity 2
                setResult(A2.RESULT_OK, returnIntent);                // with result
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(A3.this, A2.class));
        finish();
    }
}
