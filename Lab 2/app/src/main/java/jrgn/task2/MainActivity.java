package jrgn.task2;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import java.util.ArrayList;
import android.app.ListActivity;

public class MainActivity extends ListActivity {

    ArrayList<String> listContent = new ArrayList<String>();
    ArrayAdapter<String> adapter;
    ArrayList<RSSHelper> feed = new ArrayList<RSSHelper>();
    private boolean active = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listContent);
        setListAdapter(adapter);

        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(feed.get(i).link));
                startActivity(intent);
            }
        });

        if (!GetRSS.isActive()) {
            Intent intent = new Intent(this, GetRSS.class);
            startService(intent);
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (active) {
                    try {
                        Log.d("MainActivity", "Sleeping");
                        Thread.sleep(500);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    if (GetRSS.updated()) {
                        Log.d("MainActivity", "Collecting new content");
                        refresh();
                    }
                }
            }
        }).start();
    }

    public void OnButtonClick(View view) {
        Intent intent = new Intent(this, PreferencesActivity.class);
        startActivity(intent);
    }

    public void refresh() {
        new GetContentTask().execute((Void) null);
    }

    @Override
    protected void onStop(){
        super.onStop();
        active = false;
    }

    private class GetContentTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... voids) {
            Boolean success = false;
            while (!success) {
                success = GetRSS.getRSSContent(feed);
                try {
                    Thread.sleep(50);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            listContent.clear();
            for (RSSHelper item : feed) {
                listContent.add(item.title);
            }
            return success;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (success)
                adapter.notifyDataSetChanged();
        }
    }
}
