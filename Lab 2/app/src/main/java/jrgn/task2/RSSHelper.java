package jrgn.task2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

    // Helper class to hold title, link and description information
public class RSSHelper {
    public String title;
    public String link;

    public RSSHelper(String title, String link) {
        this.title = title;
        this.link = link;
    }
}