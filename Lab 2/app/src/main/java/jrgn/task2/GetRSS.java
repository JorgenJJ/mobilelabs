package jrgn.task2;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

public class GetRSS extends Service {

    private static String rssUrl;
    private static int limit;
    private static int frequency;
    private static long mill;
    private static GetRSS this2;
    private static boolean active = false;
    private static ArrayList<RSSHelper> rssContent;
    private static boolean newItems = false;

    @Override
    public void onCreate() {
        super.onCreate();

        rssContent = new ArrayList<>();
        active = true;
        this2 = this;
        getPreferences();

        new Thread(new FetchThread()).start();

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        return Service.START_STICKY;
    }

    public static void getPreferences() {
        SharedPreferences sharedpreferences = PreferenceManager.getDefaultSharedPreferences(this2);
        rssUrl = sharedpreferences.getString("url", "https://www.nrk.no/toppsaker.rss");
        limit = sharedpreferences.getInt("limit", 20);
        frequency = sharedpreferences.getInt("frequency", 1);
    }

    public static boolean getRSSContent(ArrayList<RSSHelper> o) {
        o.clear();
        if (rssContent != null) {
            synchronized (rssContent) {
                for (RSSHelper item : rssContent) {
                    o.add(item);
                }
                newItems = false;
            }
            return true;
        }
        return false;
    }

    private class FetchThread implements Runnable {
        public void run() {
            while(active) {
                try {
                    fetch();
                    while (mill < frequency * 1000 * 60) {
                        Thread.sleep(500);
                        mill += 500;
                        if (mill % 1000 == 0) {
                            Log.d("GetRSS", "Time until refresh: " + Long.toString(frequency * 60 - mill / 1000) + " seconds");
                        }
                    }
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void fetch() {
        try {
            URL url = new URL(rssUrl);
            InputStream input = url.openConnection().getInputStream();
            readContent(input);
        }
        catch (IOException e) {
            Log.e("GetRSS", "ERROR!", e);
        }
        catch (XmlPullParserException e) {
            Log.e("GetRSS", "ERROR!", e);
        }
    }

    private void readContent(InputStream input) throws XmlPullParserException, IOException {
        String title = null;
        String link = null;
        boolean isItem = false;

        try {
            XmlPullParser xmlPullParser = Xml.newPullParser();
            xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            xmlPullParser.setInput(input, null);

            xmlPullParser.nextTag();
            synchronized (rssContent) {
                while (xmlPullParser.next() != XmlPullParser.END_DOCUMENT) {
                    int eventType = xmlPullParser.getEventType();

                    String name = xmlPullParser.getName();
                    if (name == null)
                        continue;

                    if (eventType == XmlPullParser.END_TAG) {
                        if (name.equalsIgnoreCase("item") || name.equalsIgnoreCase("entry"))
                            isItem = false;
                        continue;
                    }

                    if (eventType == XmlPullParser.START_TAG) {
                        if (name.equalsIgnoreCase("item") || name.equalsIgnoreCase("entry")) {
                            isItem = true;
                            continue;
                        }
                    }

                    String res = "";
                    if (xmlPullParser.next() == XmlPullParser.TEXT) {
                        res = xmlPullParser.getText();
                        xmlPullParser.nextTag();
                    }

                    if (name.equalsIgnoreCase("title") && isItem) {
                        Log.d("GetRSS", "Parsing: " + res);
                        title = res;
                    }
                    else if ((name.equalsIgnoreCase("link") || name.equalsIgnoreCase("id")) && isItem) {
                        Log.d("GetRSS", "Parsing: " + res);
                        link = res;
                    }

                    if (title != null && link != null && !link.isEmpty()) {
                        if (isItem) {
                            boolean newItem = true;
                            for (RSSHelper item : rssContent) {
                                if (item.link.contentEquals(link)) {
                                    newItem = false;
                                }
                            }
                            if (newItem) {
                                newItems = true;
                                RSSHelper item = new RSSHelper(title, link);
                                rssContent.add(0, item);
                            }
                        }

                        title = null;
                        link = null;
                        isItem = false;
                    }
                }

                while (rssContent.size() > limit) {
                    rssContent.remove(rssContent.size() - 1);
                }
            }
        }

        finally {
            input.close();
        }
    }

    @Override
    public void onDestroy() {
        active = false;
        mill = 0;
    }

    public static boolean updated(){
        return newItems;
    }

    public static boolean isActive(){
        return active;
    }

}

