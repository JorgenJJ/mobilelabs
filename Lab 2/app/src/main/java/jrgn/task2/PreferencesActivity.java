package jrgn.task2;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class PreferencesActivity extends AppCompatActivity {

    EditText url;       // Link to RSS feed
    EditText limit;     // Limit on how many items can be displayed
    EditText frequency; // How often the app should fetch new content

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);

        url = findViewById(R.id.url);
        limit = findViewById(R.id.limit);
        frequency = findViewById(R.id.frequency);

        final Activity activity = this;

        SharedPreferences sharedpreferences = PreferenceManager.getDefaultSharedPreferences(activity);
        url.setText(sharedpreferences.getString("url", "https://www.nrk.no/toppsaker.rss"));
        limit.setText(Integer.toString(sharedpreferences.getInt("limit", 20)));
        frequency.setText(Integer.toString(sharedpreferences.getInt("frequency", 1)));

        url.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    SharedPreferences sharedpreferences = PreferenceManager.getDefaultSharedPreferences(activity);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("url", url.getText().toString());
                    editor.commit();
                }
            }
        });

        limit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    SharedPreferences sharedpreferences = PreferenceManager.getDefaultSharedPreferences(activity);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("limit", (Integer.parseInt(limit.getText().toString())));
                    editor.commit();
                }
            }
        });

        frequency.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    SharedPreferences sharedpreferences = PreferenceManager.getDefaultSharedPreferences(activity);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("frequency", (Integer.parseInt(frequency.getText().toString())));
                    editor.commit();
                }
            }
        });
    }

    public void saveSettings(View view) {
        SharedPreferences sharedpreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("url", url.getText().toString());
        editor.putInt("limit", (Integer.parseInt(limit.getText().toString())));
        editor.putInt("frequency", (Integer.parseInt(frequency.getText().toString())));
        editor.commit();
    }
}
